# brightnessctl - Control screen brightness on Linux
Inspired by [brightnessctl](https://github.com/Hummer12007/brightnessctl) <3

![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/mattclement/brightnessctl/master.svg)
[![PyPI license](https://img.shields.io/pypi/l/brightnessctl.svg)](https://pypi.python.org/pypi/brightnessctl/)
[![PyPI version shields.io](https://img.shields.io/pypi/v/brightnessctl.svg)](https://pypi.python.org/pypi/brightnessctl/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/brightnessctl.svg)](https://pypi.python.org/pypi/brightnessctl/)

# Installation
- `pip install --user brightnessctl`
- Copy the [udev rules](https://gitlab.com/mattclement/blob/master/90-brightness.rules) to `/etc/udev/rules.d` and load
- Add your user to the `video` group: `sudo gpasswd -a <user> video`

# Usage
```sh
usage: brightnessctl [-h] [-v] [-p N | -d N | -r N] [--duration S]
                     [--save | --restore]

optional arguments:
  -h, --help          show this help message and exit
  -v, --verbose
  -p N, --percent N   Set brightness to <N> %
  -d N, --delta N     Adjust brightness by <N> % of max brightness
  -r N, --relative N  Adjust brightness by <N> % of current brightness
  --duration S        How long to take to get to final value
  --save              Save current value to disk before changing value
  --restore           Restore saved value from disk
```
