import io
from unittest import TestCase
from unittest.mock import patch, Mock, mock_open, MagicMock
from brightnessctl.brightnessctl import Brightness


class MockBacklightFS(object):
    """ Mocks for the /sys/class/backlight/intel_backlight filesystem. """
    def __init__(self):
        calls = ["max_brightness", "actual_brightness",
                 "brightness", "backlight"]
        for call in calls:
            setattr(self, call, MagicMock())

    def route(self, *args, **kwargs):
        return getattr(self, args[0].name)


class TestBrightnessController(TestCase):
    @patch("builtins.open")
    def test_current_percent(self, backlight):

        def side_effect(file, **kwargs):
            if file.name == "actual_brightness":
                return io.StringIO("500")
            if file.name == "max_brightness":
                return io.StringIO("1000")

        backlight.side_effect = side_effect
        b = Brightness()

        self.assertFalse(backlight.called)
        self.assertEqual(b.current_percent, 50.0)
        self.assertTrue(backlight.called)

    @patch("builtins.open")
    def test_set(self, backlight):
        mocks = MockBacklightFS()
        backlight.side_effect = mocks.route

        mocks.max_brightness.__enter__.return_value = io.StringIO("1000")

        b = Brightness()
        brightness = mocks.brightness

        b.set(500)
        brightness.__enter__.return_value.write.assert_called_with("500")

        # Bound at zero brightness
        b.set(-100)
        brightness.__enter__.return_value.write.assert_called_with("0")

        # Bound at max brightness
        b.set(5000)
        brightness.__enter__.return_value.write.assert_called_with("1000")

    @patch("brightnessctl.brightnessctl.sleep")
    @patch("builtins.open")
    def test_stepping(self, backlight, sleep):
        mocks = MockBacklightFS()
        backlight.side_effect = mocks.route
        mocks.max_brightness.__enter__.return_value = io.StringIO("1000")

        b = Brightness()
        brightness = mocks.brightness

        b.smooth_step(0, 1)

        # Should end at zero brightness, and took 150 steps to get there
        brightness.__enter__.return_value.write.assert_called_with("0")
        self.assertEqual(
            brightness.__enter__.return_value.write.call_count,
            150
        )




if __name__ == "__main__":
    import unittest
    unittest.main()
